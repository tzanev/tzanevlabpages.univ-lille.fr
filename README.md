# [Kroum Tzanev](https://gitlabpages.univ-lille.fr/tzanev)

<img src="images/kroum_2022.09.jpg" width="200" height="200" alt="Kroum Tzanev" />

## Coordonnées

- Mail :	`prenom.nom@univ-lille.fr` (avec `prenom=kroum` et `nom=tzanev`)<br>
- Téléphone :
  - `[ bur ] 03.20.43.44.91`
  - `[ dom ] 03.61.26.20.09`

## Enseignements

### 2024–2025

- [M53 - « Intégrales à paramètres et séries de Fourier »](https://gitlabpages.univ-lille.fr/m53lille) de la L3 Mathématiques
- [M61B - « Probabilités-Intégration »](https://gitlabpages.univ-lille.fr/tzanev/l3m61proba/) de la L3 Mathématiques
- [M1 - « Analyse »](https://gitlabpages.univ-lille.fr/tzanev/m1analyse) du M1 Mathématiques
- [M1 - « Informatique »](https://gitlabpages.univ-lille.fr/tzanev/m1info) du M1 Mathématiques
- [M1 - « LaTeX »](https://gitlabpages.univ-lille.fr/meef-math/m1latex) du M1 MEEF


### 2023–2024

- [M53 - « Intégrales à paramètres et séries de Fourier »](https://gitlabpages.univ-lille.fr/m53lille/2023/) de la L3 Mathématiques
- [M61B - « Probabilités-Intégration »](https://gitlabpages.univ-lille.fr/tzanev/l3m61proba/2024/) de la L3 Mathématiques
- [M1 - « Analyse »](https://gitlabpages.univ-lille.fr/tzanev/m1analyse/2023) du M1 Mathématiques
- [M1 - « Informatique »](https://gitlabpages.univ-lille.fr/tzanev/m1info/2024) du M1 Mathématiques

### 2022–2023

- [M44 - « Géométrie »](https://ktzanev.gitlab.io/m44lille/) de la L2 Mathématiques
- [TeX4 - « LaTeX »](https://ktzanev.gitlab.io/tex4lille/) de la L2 Mathématiques
- [M62 – « Équations différentielles », TPs](https://ktzanev.github.io/m62lille/) de la L3 Mathématiques
- [M1 - « Analyse »](https://gitlabpages.univ-lille.fr/tzanev/m1analyse/2022) ! du M1 Mathématiques
- [OPM - « LaTeX »](https://ktzanev.gitlab.io/m1opm-tex/) du M1 Mathématiques

### 2021–2022

- [M44 - « Géométrie »](https://ktzanev.gitlab.io/m44lille/2022/) de la L2 Mathématiques
- [TeX4 - « LaTeX »](https://ktzanev.gitlab.io/tex4lille/2022/) de la L2 Mathématiques
- [M62 – « Équations différentielles », TPs](https://ktzanev.github.io/m62lille/archives.html#202122) de la L3 Mathématiques
- [Probability 1](https://ktzanev.gitlab.io/m1dslille/) du M1 Data Science - Université de Lille et Centrale Lille (en anglais)
- [OPM - « LaTeX »](https://ktzanev.gitlab.io/m1opm-tex/2022/) du M1 Mathématiques

### 2020–2021

- [M44 - « Géométrie »](https://ktzanev.gitlab.io/m44lille/2021/) de la L2 Mathématiques
- [TeX4 - « LaTeX »](https://ktzanev.gitlab.io/tex4lille/2021/) de la L2 Mathématiques
- [M62 – « Équations différentielles », TPs](https://ktzanev.github.io/m62lille/archives.html#202021) de la L3 Mathématiques
- [Probability 1](https://ktzanev.gitlab.io/m1dslille/2020/) du M1 Data Science - Université de Lille et Centrale Lille (en anglais)
- [OPM - « LaTeX »](https://ktzanev.gitlab.io/m1opm-tex/2021/) du M1 Mathématiques

### 2019–2020

- M34 - « Analyse numérique » de la L2 Mathématiques
- [M52 - « Topologie »](https://ktzanev.gitlab.io/m52lille/) de la L3 Mathématiques
- [M62 – « Équations différentielles », TPs](https://ktzanev.github.io/m62lille/archives.html#201920) de la L3 Mathématiques
- [M67 - « Géométrie élémentaire »](https://ktzanev.github.io/m67lille/) de la L3 Mathématiques
- [Probability 1](https://ktzanev.gitlab.io/m1dslille/2019/) du M1 Data Science - Université de Lille et Centrale Lille (en anglais)

### 2018–2019

- [M3 - « Probabilités »](https://gitlabpages.univ-lille.fr/tzanev/l2m3proba/) du Parcours renforcé L2 Physique
- [M52 - « Topologie »](https://ktzanev.gitlab.io/m52lille/2018/) de la L3 Mathématiques
- [M54 - « Probabilités »](https://ktzanev.gitlab.io/m54lille/) de la L3 Mathématiques
- [M67 - « Géométrie élémentaire »](https://ktzanev.github.io/m67lille/#201819) de la L3 Mathématiques

### 2017–2018

- [M4 - « Probabilités et fonctions »](https://ktzanev.github.io/m4prlille/) du Parcours renforcé L2 Physique
- M52 - « Topologie » de la L3 Mathématiques
- [M53 - « Géométrie affine et euclidienne »](https://ktzanev.github.io/m53lille1/) de la L3 Mathématiques
- [M67 - « Géométrie élémentaire »](https://ktzanev.github.io/m67lille/#201718) de la L3 Mathématiques

### 2016–2017

- [M31 - « Algèbre linéaire »](https://gitlabpages.univ-lille.fr/tzanev/l2m31alglin/) de la L2 Mathématiques
- [M43 - « Probabilités discrètes »](https://ktzanev.github.io/m43lille1/) de la L2 Mathématiques
- [TeX4 - « LaTeX et HTML »](https://ktzanev.github.io/tex4lille1/) de la L2 Mathématiques
- [M53 - « Géométrie affine et euclidienne »](https://ktzanev.github.io/m53lille1/2016/) de la L3 Mathématiques

### 2015–2016

- M32 - « Fonctions de plusieurs variables » de la L2 Mathématiques
- [M43 - « Probabilités discrètes »](https://ktzanev.github.io/m43lille1/#201516) de la L2 Mathématiques
- [M53 - « Géométrie affine et euclidienne »](https://ktzanev.github.io/m53lille1/2015/) de la L3 Mathématiques
- [TeX4 - « LaTeX et HTML »](https://github.com/ktzanev/tex4lille1/tree/v2016) de la L2 Mathématiques

## Autres

- [Info pour les nuls](https://gitlabpages.univ-lille.fr/math-info-lesnuls/) : cycle d'exposés à destination des collègues.
- [www.mathconf.org](https://www.mathconf.org/) : gestion de sites de conférences de mathématiques.\
  _(Il faut me contacter pour la création d'un évènement. [Quelques exemples pour 2025](https://www.google.com/search?q=site%3Awww.mathconf.org+2025).)_
- [Le logo du labo](https://ktzanev.github.io/logolabopp/) : le logo du laboratoire Paul Painlevé, ainsi que des lettres types utilisant ce logo  et quelques autres logos universitaire.
- [Lettre du département](https://gitlabpages.univ-lille.fr/tzanev/math-ulille-lettre/) : un modèle LaTeX de lettre du département de mathématiques de l’Université de Lille.
- [Plans du bâtiment M1](https://ktzanev.github.io/m1lille1/) : la plans du bâtiment M1 au Cité Scientifique de Villeneuve d'Ascq.
- [Planning pour LaTeX](https://ktzanev.github.io/planning/) : un package LaTeX pour dessiner l'emploi du temps d'une formation.
- [Venir au labo](https://ktzanev.github.io/venir-labopp/) : site qui décrit comment venir au labo de math de Lille.
- [ApogIE](https://ktzanev.github.io/apogie/) : site qui facilite l'exportation depuis et l'importation vers Apogée.
- [Blog](https://ktzanev.github.io/blog/) : quelques articles dont je ne sais pas où les mettre.
- [<img src="images/gitlab.svg" height="17" style="vertical-align: middle"> Le dépôt git de ma page](https://gitlab.univ-lille.fr/tzanev/tzanev.gitlabpages.univ-lille.fr).
